## haakon.dev

Redesigning my portfolio for the *nth* time. 

### Technologies / framework

This code structure is something I've picked up while working on a project at Equinor.
* React
* Typescript
* styled-components

I've really enjoyed working with these technologies and this file-structure and will probably continue to do so on personal projects for the forseeable future. 

### Design

This design is something I'm sketching "as-I-go" using some nice shades of purple & pink - mostly inspired by [this pen](https://codepen.io/schart/pen/vYGGvJo) that I created a few days before starting the project.
